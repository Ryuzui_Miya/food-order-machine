import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FoodOrderMachine {
    public static int x=0;
    void order(String food,int j) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering! It will be served " +
                    "as soon as possible.");
            String currenText = textPane1.getText();
            textPane1.setText(currenText + food + "" + j + "yen" + "\n");
            x += j;
            goukei.setText("Total " + x + " yen");
        }
    }
    private JPanel root;
    private JLabel toplabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton yakisobaButton;
    private JButton udonButton;
    private JButton tyaHanButton;
    private JButton karaageButton;
    private JLabel menu;
    private JTextPane textPane1;
    private JButton cheakOutButton;
    private JLabel goukei;

    public static void main(String[] args){
        JFrame frame=new JFrame("Food-Order-Machine");
        frame.setContentPane(new FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodOrderMachine(){
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura ",200);
            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura2.png")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen ",300);
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen2.png")));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba ",250);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("yakisoba2.png")));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon ",150);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon2.png")));

        tyaHanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tya-han ",130);
            }
        });
        tyaHanButton.setIcon(new ImageIcon(this.getClass().getResource("tya-han2.png")));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage ",100);
            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("karaage2.png")));

        cheakOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "would you like to checkout?",
                        "Order confimation",
                JOptionPane.YES_NO_OPTION);

                if(confirmation==0){
                    textPane1.setText(null);
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+x+" yen.");
                    x=0;
                    goukei.setText("Total "+x+" yen.");
                }
            }
        });

    }
   }



